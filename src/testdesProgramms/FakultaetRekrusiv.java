package testdesProgramms;

public class FakultaetRekrusiv {

	public static int rechneFakul(int a) {
		if (a <= 1) {
			return 1;
		} else {
			return a * rechneFakul(a - 1);
		}
	}

	static void berechneFakul(int number) {
		int fakult = 1;
		for (int i = 1; i <= number; i++) {
			fakult *= i;
		}
		System.out.println("Di efaku von zahl ist: " + fakult);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(rechneFakul(30));
		berechneFakul(4);
	}

}
