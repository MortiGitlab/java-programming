package testdesProgramms;

public class WrapperClass {

//	static void p(String s) {
//		
//		System.out.println(s);
//	}
//	

	
	/**
	 * 
	 * 
	 * @param 
	 * 
	 * */
	static void m(int n) {

		n = n - 2;
		System.out.println(n);
	}

	static void sum(int[] n) {

		int s = 0;

		for (int idx = 0; idx < n.length; idx++) {
			s += n[idx];
			n[idx] = 0;

		}
 
		System.out.println(s);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// call by value (aufruf durch kopie) und reference
		int[] a = { 1, 2, 3, 4, };

		//m(42);
		sum(a);
		sum(a);
		

//		Integer i = 43;
//		int x = i; // unboxing 
//		p(i.toString());
//		

	}

}
