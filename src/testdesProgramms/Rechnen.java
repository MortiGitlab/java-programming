package testdesProgramms;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Rechnen {

    public static void main(String[] args) {
        Teiler teiler = new Teiler();
        teiler.setDividend(26);
        teiler.setDivisor(3);
        teiler.teile();
        System.out.println(teiler.getErgebnis());
    }
}

class Teiler {
    private double dividend = 0, divisor = 1, ergebnis;

    public void teile() {
        ergebnis = dividend / divisor;
    }

    public void setDividend(double d) {
        dividend = d;
    }

    public void setDivisor(double d) {
        divisor = 1;
        if (d != 0) {
            divisor = d;
        }
    }

    public double getErgebnis() {
        NumberFormat format = NumberFormat.getNumberInstance(Locale.US);
        ((DecimalFormat) format).applyPattern("#.##");
        String dStr = format.format(new Double(ergebnis));
        return new Double(dStr).doubleValue();
    }
}