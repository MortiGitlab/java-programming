package testdesProgramms;

import java.text.NumberFormat;

public class Runden {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double number = 1234.565;
		System.out.println(number + " gerundet: " + String.format("%1.2f �", number));

		double number1 = 1234.565;
		NumberFormat formatter = NumberFormat.getInstance();
		formatter.setMaximumFractionDigits(2);
		System.out.println(number1 + " gerundet: " + formatter.format(number1));
	}

}
