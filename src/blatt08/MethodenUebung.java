package blatt08;

import java.text.DecimalFormat;
import java.util.Arrays;

public class MethodenUebung {

	/**
	 * @author morti
	 * @param int n
	 * @return gibt Betrag einer Zahl aus.Positiver Wert einer eingegebenen zahl
	 */
	public static int AmountOfNumber(int n) {
		if (n >= 0) {
			return n;
		} else {
			return (n) * (-1);
		}

	}

	/**
	 * @author morti
	 * @return Aufrunden einer Zahl einfacher l�sen int 24,2=24
	 */

	public static double roundUpToTheWholeNumber(double zahl) {

		if (zahl % 1 == 0) {
			return (int) zahl;

		} else {
			if (zahl >= 0) {
				DecimalFormat formatter = new DecimalFormat("#");
				zahl = Double.parseDouble(formatter.format(zahl));
				return (int) zahl;

			} else {
				DecimalFormat formatter = new DecimalFormat("#");
				zahl = Double.parseDouble(formatter.format(zahl));
				return (int) zahl;
			}
		}

	}

	/**
	 * @author morti
	 * @return Abrunden einer Zahl
	 */

	public static double roundDownToTheWholeNumber(double zahl) {

		if (zahl >= 0) {
			DecimalFormat formatter = new DecimalFormat("#");
			zahl = Double.parseDouble(formatter.format(zahl)) - 1;
			return (int) zahl;

		} else {
			DecimalFormat formatter = new DecimalFormat("#");
			zahl = Double.parseDouble(formatter.format(zahl));
			return (int) zahl;
		}

	}

	/**
	 * @return ob eine Zahl restlos durch eine andere Zahl teilbar ist
	 */
	public static boolean divisibility(double number1, double number2) {

		return (number1 % number2 == 0);

	}

	/**
	 * Euklidische Distanz
	 * 
	 * @return Abstand zwischen 2 Punkten
	 */

	public static double getDistance(double xP1, double yP1, double xP2, double yP2) {
		return Math.sqrt(Math.pow((xP2 - xP1), 2) + Math.pow((yP2 - yP1), 2));
	}

	/**
	 * @return smallest number of any three numbers
	 * 
	 */
	public static double smallestNumberOfAnyThreeNumbers(double a, double b, double c) {
		double[] input = { a, b, c, };
		Arrays.sort(input);
		return input[0];

	}

	/**
	 * 
	 * Die Zahlen Reihe nach ausgeben
	 */

	public static void prt1234(long n) {
		if (n >= 1) {//rekrusionsbasis

			prt1234(n - 1);//rekrusionsfortsetzung

			System.out.print(n + ", ");
		}
//		System.out.print(n + ", ");

	}

	/**
	 * @return Rekursion f�r die Folge 1, 4, 9, ...,, n*n,
	 */
	public static void prtSqr1234(long n) {
		if (n >= 1) {

			prtSqr1234(n - 1);
			System.out.print(n * n + ", ");

		}

	}

	/**
	 * @return die geraden Zahlen 2, 4, 6, ..., n, ausgeben
	 */

	public static void prt2468(long n) {

		if (n >= 1) {
			prt2468(n - 1);
			if (n % 2 == 0) {
				System.out.print(n + ",");

			} 

		}

	}

	void xxx(long n) {
		if (n > 0L) {// Rekrusionsbasis
			System.out.println(n);
			xxx(n - 1);
		} else {// Rekursionsfortsetzung
		}
	}

	/**
	 * @return eine iterative Variante der Methode xxx
	 */

	public static void xxxIteration(long n) {
		for (; n > 0; n--) {
			System.out.print(n + ",");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("1. Betrag einer beliebigen Zahl: " + AmountOfNumber(-10));
		System.out.println("2. Eine Zahl aufrunden zur ganzen Zahl: " + roundUpToTheWholeNumber(234.76545));
		System.out.println("3. Ebenso abrunden zur ganzen Zahl: " + roundDownToTheWholeNumber(234.76545));
		System.out.println("4.ob eine Zahl restlos durch andere Zahl teilbar ist? " + divisibility(12, 6));
		System.out.println("5.den Abstand zweier beliebiger Punkte:" + getDistance(2, 3, 4, 5));
		System.out.println("6.die kleinste Zahl dreier beliebigen Zahlen: " + smallestNumberOfAnyThreeNumbers(2, 5, 8));
		prt1234(4);
		System.out.println();
		prtSqr1234(7);
		System.out.println();
		// prt2468(6);
		xxxIteration(4);

	}

}
