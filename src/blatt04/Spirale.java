package blatt04;

import java.awt.*;
import javax.swing.JFrame;

/**
 * Klasse f�r das Zeichnen eines Sterns
 */
public class Spirale extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	/** Konstruktor. Nichts zu �ndern hier */
    public Spirale(){
        super();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(900,768);
        this.setTitle("L�sung in der Klasse " + this.getClass().getName()); 
        this.setVisible(true);
    }

    /** Main-Methode. Nichts zu �ndern hier. */
    public static void main(String[] args){
    	new Spirale();
    }
   
    /** Paint-Methode. Nichts zu �ndern hier. Diese Methode 
     *  wird beim �ffnen des Fensters automatisch aufgerufen. 
     *  Die Methode wird auch aufgerufen, wenn die Gr��e des Fensters
     *  ver�ndert wird. 
     *  @param g Grafik, auf die gezeichnet wird.
     *  */
   
    public void zeichneEinenStern(Graphics g,int x1,int y1,int n, int r) {
 	   
 	   for(int i=0;i<(n-1);i++) {

 			double phi = (i * (2 * Math.PI) / n);//berechnung Schwarze Linie bzw Punkt 
 			double phi2 = (Math.PI) / n;// berechnung der rote linie bzw punkt
 			
 			int x2 = ((int) (r * Math.cos(phi))) + x1;
 			int y2 = ((int) (r * Math.sin(phi))) + y1; 
 			int x3 = ((int) -(r * Math.cos(phi))) + x1;
 			int y3 = ((int) -(r * Math.sin(phi))) + y1;
 			
 			int x4 = ((int) (r / 2 * Math.cos(phi + (phi2)))) + x1;
 			int y4 = ((int) (r / 2 * Math.sin(phi + (phi2)))) + y1;
 			int x5 = ((int) -(r / 2 * Math.cos(phi + (phi2)))) + x1;
 			int y5 = ((int) -(r / 2 * Math.sin(phi + (phi2)))) + y1;

 			g.setColor(Color.BLUE);
 			g.drawLine(x2, y2, x3, y3);
 			g.setColor(Color.RED);
 			g.drawLine(x4, y4, x5, y5);
 		   
 		   
 		   
 		
 		   
 	   }
    }
  
    public void paint(Graphics g){
    	
    	double breite = this.getWidth()/2 ;
    	double laenge = this.getHeight() /3;
    	double r = breite ;
    	final int anzahlDerSterne = 12;
    	
    	
    	for(int i=0; i < anzahlDerSterne; i++)
    	{
    	double phi =  Math.PI / anzahlDerSterne * i;
    	double x = Math.cos(phi) * r / anzahlDerSterne * (anzahlDerSterne-i);
    	double y = Math.sin(phi) * r / anzahlDerSterne * (anzahlDerSterne-i);
    	this.zeichneEinenStern(g, (int)(breite + x), (int)(laenge + y), (int)(r /
    	(i+1)), i);
    	}
    	}
  
   }