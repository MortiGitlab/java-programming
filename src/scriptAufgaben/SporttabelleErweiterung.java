package scriptAufgaben;

/**
 * Gegeben: Startjahr, Anzahl Jahre gesucht : Tabelle, ob Großsportereignis
 * ist.Nutzen sie bitte den %-Operator, um festzustellen, ob eine Jahresza
 * gerade ist.
 */

public class SporttabelleErweiterung {

	/**
	 * Wie gross ist ein Block (i.d.R. 3) im Sudoku.<br>
	 * <br>
	 * <b>WARNUNG:</b> Es reicht nicht aus, einfach diese Zahl zum Beispiel fuer
	 * 16x16 Sudokus hochzusetzen. Da ist einiges mehr zu tun ...
	 */

	private final static int BLOCK_SIZE = 3;

	
	/**
	 * 
	 * @author morti
	 * 
	 * 
	 * */
	int x = 0;
	
	/**
	 * 
	 * @param i Anzahl der Jahren 
	 * @return die Jahren 
	 * @author morteza Hosseini 
	 * */

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
		

		for (int i = 2018; i < 2029; i++) {

			if (i % 2 == 0 && i % 4 == 0) {
				System.out.println(i + " gibt es eine Sommerolympiade");

			}

			else if (i % 2 == 0 && i % 4 != 0) {
				System.out.println(i + "  🙂 gibt es eine Fußbalweltmeisterschaft");

			}
		}
		System.out.println(BLOCK_SIZE);
	}

}
