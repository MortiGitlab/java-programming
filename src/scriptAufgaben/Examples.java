package scriptAufgaben;

public class Examples {

	public static void main(String[] args) {

		System.out.println("Einen schönen Tag! - 좋은 하루 되세요!");

		int bröcklsHassVariable = 42;
		double 变化 = 2.0;
		System.out.println(bröcklsHassVariable * 变化);

		// ############ Zahlenkonstanten ################

		int dec77 = 77; // Dezimalkonstante
		System.out.println(dec77);

		int bin111 = 0b111; // Binärkonstante
		System.out.println(bin111);

		int oct77 = 077; // Oktalkonstante
		System.out.println(oct77);

		int hex77 = -0x77; // Hexadezimalkonstante
		System.out.println(hex77);

		hex77 = 0xFE; // Hexadezimalkonstante
		System.out.println(hex77);

		// int nogoBin = 0b22;
		// int nogoOct = 0888;
		// int nogoDec 12E;

		// ############ Zahlenkonstanten für longs ###############

		long lMinusOne = 0xFFFF_FFFF_FFFF_FFFFL; // wg. Zweierkomplement
		System.out.println(lMinusOne);

		long lmax = 0x7FFF_FFFF_FFFF_FFFFL; // "_": Visuelles Trennglied
		System.out.println(lmax);
		System.out.println(Long.MAX_VALUE);

		lmax = 12; // Schlecht: Automatische Typwandlung nötig
		lmax = 12l; // Geht, aber Typ-Suffix "l" schlecht lesbar
		lmax = 12L; // Geht und gut lesbar

		// ############ Gleitpunktkonstanten ################

		float f = 123.45e10f;
		// Mantisse ------
		// Exponent ---
		// Typ Suffix - // Kein Suffix -> double
		f = 1.2345_6789_01234_5678f;
		System.out.printf("%20.16f\n", f);

		f = 12;
		// nogo f = 12.;
		double d = 12.f; // Schlecht: Automatische Typwandlung nötig

		// ############ Zeichenkonstanten ################

		char c = 'A';
		System.out.println(c + 0);

		c = 'a' + 1;
		System.out.println(c);

		for (c = 'a'; c - 'a' < 26; c = (char) (c + 1)) {
			System.out.printf("%c ", c);
		}

		// ############ Ersatzdarstellungen ################

		// Ersatzdarstellung wie \n zählen als ein Zeichen:
		System.out.println("\n\na\nb\nc");
		System.out.println("\na\tb\tc");
		System.out.println("c:\\TEMP\\blah.txt");
		c = '\'';
		System.out.println(c);

		System.out.println("\u2192 \u2191 \u2190 \u2666 \u1f600");

		String s = "Kette";
		System.out.println(s);

	}

}
