package scriptAufgaben;

public class SpielKarten {
	public static final int MAX_CARDS = 32;// symbolische Konstante
	public static final int NUM_RED_CARDS = MAX_CARDS / 2;
	// ...
	Karte[] karten = new Karte[SpielKarten.MAX_CARDS];
	Karte[] roteKarten = new Karte[SpielKarten.NUM_RED_CARDS];
	Karte[] schwarzeKarten = new Karte[SpielKarten.MAX_CARDS - SpielKarten.NUM_RED_CARDS];

	public static void main(String[] args) {
		System.out.println("Es gibt " + SpielKarten.MAX_CARDS + " Karten");
		System.out.println("Pi ist " + Math.PI + " ! ");
	}
}

class Karte {
}
