	
	// Datei Person.java
	package blatt05;
	
	/**
	 * Einfache Klasse f�r Personen
	 */
	public class Person {
		//Konstruktoren, Methoden und Felder, die mit dem Modifikator protected gekennzeichnet sind, 
		//stehen auch in von der deklarierenden Klasse abgeleiteten Klassen anderer Packages zur Verf�gung.
		protected String vorname;
		protected String nachname;
	
		/**
		 * Konstruktor
		 * @param vorname
		 * @param nachname
		 */
		public Person(String vorname, String nachname) {
			this.vorname = vorname;
			this.nachname = nachname;
		}
	
		
		public void print() {
			System.out.printf("%20s, %-10s", this.nachname, this.vorname);
		}
		/**
		 * Tests
		 * @param args unused
		 */
		public static void main(String[] args) {
			Person lin = new Person("Lin", "May");
			lin.print();
			Person k = new Person("Kai", "Hai");
			System.out.println(k.nachname);
			k.print();
		}
	}