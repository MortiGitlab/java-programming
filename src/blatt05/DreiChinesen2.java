package blatt05;

import java.util.ArrayList;

public class DreiChinesen2 {
	static final String text = "Drei Chinesen mit dem Kontrabass\n" + "sa�en auf der Stra�e und erz�hlten sich was.\n"
			+ "Da kam ein Mann: Ja was ist denn das?\n" + "Drei Chinesen mit dem Kontrabass.\n\n";

	/**
	 * Einen Text zentriert ausgeben. Es gen�gt eine L�sung f�r nichtproportionale
	 * Schriftarten.
	 * 
	 * @param text Text mit mehreren Zeilen (durch '\n' abgetrennt)
	 */

	public static void printChars(char c, int len) {
		for (int i = 0; i < len; i++) {
			System.out.print(c);
		}
	}

	public static void printCentered(String zeichnekette) {

		// zeichnekette.length();// hat keinen Wert !! liefert nur gr��e
		String[] lines = zeichnekette.split("\n");

		// immer mit if geschweiften Klammer ohne man kann nur einen Befehl f�hren!!!!!!
		int max = 0;
		for (String line : lines) {
			if (line.length() > max) { // sp�ter ausprobieren 
				max = line.length();				
			}

		}

		max += 0;

		// +++++++++++Ab hier werden die linien mit 194 abstand ausgegeben.+++++++++

		for (String line : lines) {
			printChars(' ', (max - line.length()) / 2);
			System.out.println(line);

		}

	}

// nicht viele Leerzeilen, den Blick verliert man ganz schnell !!!!! entweder kein 

	/**
	 * Vokale in Text ersetzen.
	 * 
	 * @param text        Text mit Vokalen
	 * @param replacement Ersatz f�r Vokale
	 * @return
	 * @return Text mit ge�nderten Vokalen
	 */
	public static String changeVowels(String lines) {

		lines = lines.replaceAll("[aeou�]", "i");// in java Dokumentation reinschauen, was String macht.
		return lines;

	}

	public static void main(String[] args) {

		printCentered(text);
		changeVowels(text);
		printCentered(changeVowels(text));

	}

}
