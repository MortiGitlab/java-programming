package blatt05;

public class Bauer {

	private String name;//instanzvariable
	private static  int anzahlGefuetterterHuehner;// klassenvariablen

	public Bauer(String name) {
		this.name = name;// this ist die Referenz auf die eigene klasse 
		
	}

	public void fuettern(Huhn huhn) {// sinn des Objektes wiederspiegeln

		if (huhn.getHungrig() == false) {

			// satt
			System.out.println(" Huhn ist satt !! ");

		} else {
			// hungrig
			huhn.setHungrig(false);// satt
			anzahlGefuetterterHuehner++;//
		}

	}

	public void rufeHuhn(Huhn ubergabe) {

		ubergabe.getName();
		System.out.println(ubergabe.getName());

	}

	public static void berechneAnzahlHungrigerHuehner() {
		System.out.println(Huhn.getAnzahlHuehner() - anzahlGefuetterterHuehner);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Bauer bauer1 = new Bauer("walter"); // instanziieren, bauer 1 ist instanz
		Bauer bauer2 = new Bauer(" Alex");
		Huhn a = new Huhn("Heike");
		Huhn b = new Huhn("Heidrun");
		Huhn c = new Huhn("Hanelore");

		bauer2.rufeHuhn(a);
		bauer2.fuettern(a);
		bauer1.rufeHuhn(b);
		bauer1.fuettern(b);
		bauer1.rufeHuhn(c);
		bauer1.fuettern(c);

		// die Anzahl der hungrigen H�hner auf der Konsole

		Bauer.berechneAnzahlHungrigerHuehner();
		//bauer2.berechneAnzahlHungrigerHuehner();
		//berechneAnzahlHungrigerHuehner();

	}

}
