/**
 * Datei: Befragung.java
 * Erstes Java-Programmbeispiel
 */
package k01_intro;
import java.util.Scanner;



/**
 * @author Ulrich.Broeckl@hs-karlsruhe.de
 */
public class Befragung {

    /**
     * Geg.:   n Ja-/Nein-Fragen <br/>
     * Ges.:   Relative Haeufigkeiten p_i in Prozent, i = 1...n
     * @param  args unused
     */
    public static void main(String[] args) {
        int h;        // Anzahl Hoerende
        float p_i;    // Laufende Wahrscheinlichkeit in %
        
        System.out.println("Bitte Anzahl Hoerende eingeben:");
        Scanner sc = new Scanner(System.in);
        h = sc.nextInt();
        System.out.println("Es gibt " + h + " Hoerende.");

        System.out.println("1. Persoenlicher Internetanschluss bzw. -zugang? Anzahl:");
        p_i = 100.f * sc.nextInt() / h;
        System.out.println("Anteil: " + p_i + "%");

        System.out.println("2. Eigener PC? Anzahl:");
        p_i = 100.f * sc.nextInt() / h;
        System.out.println("Anteil: " + p_i + "%"+"\n");

        
        
        
        
        
      //***********************************************************************************************
        
        
        /* ... mehr Fragen */
        // hier um eine beliebeige Frage erweitert 
        
        
        /**
         * @author Morteza Hosseini
         * Vorlesung: Programmieren Java
         * Prof Ulrich Broeckl
         * Ausgabe: Sitzform in der Vorlesung angeben 
         */

        String form;
        int counter;
        System.out.println("Wie sollen die Studierenden in der Vorlesung sitzen? ");
        System.out.println("Es gibt zwei Sitzformen, einmal Rechtecksform und einmal Dreiecksform.  ");
        System.out.println("Geben Sie Ihr gew�nschte Form ein, entweder Rechteck oder Dreieck und dr�cken Sie die Enter-Taste : ");
        form=sc.next();
        System.out.println("Geben sie bitte die Anzahl der Kanten und dr�cken Sie die Enter-Taste :  "+"\n");
        counter=sc.nextInt();
        
        
        //um eine Form anzugeben braucht man zwei oder mehrere verschachtelte for schleifen 
        // Die erste for schleife dient dazu zu z�hlen 
        //zweite for Schleife dient dazu, eine leere Stelle vor Stern zu machen, um ein Gleichschenkliges Dreieck zu haben, sonst rechtwinkliges
        //dritte Schleife dient dazu Stern und den Abstand zu machen *Abstand*...
        //.equalsIgnoreCase(...) eingabe kann in allen varianten geschrieben werden wie DReieck,dReiEck usw
        
        
        if("Dreieck".equalsIgnoreCase(form) ) {
        for (int i = 0; i < counter; i++) {
            for (int k = i; k < counter; k++) {
                System.out.print(" ");
            }
            for (int j = 0; j <= i; j++) {
            
                    System.out.print("*");// Stern zeichnen
                System.out.print(" ");// F�r den Abstand zwischen den Sternch�n 
            }
            System.out.println("");// neue Zeile beginnen 
        }
        }
       
        
        //Hier ist das selbe Prinzip wie Dreieck
        
        else if("Rechteck".equalsIgnoreCase(form)) {

	    for (int i = 0; i <= counter; i++) {
			for (int j = 0; j <= counter; j++) {
				
				System.out.print("*");
				System.out.print(" ");
			}
			System.out.println();
		}  }
        
        //***********************************************************************************************
        sc.close();
    }
}

