package blatt07;

import java.util.Arrays;

import javax.swing.plaf.synth.SynthDesktopIconUI;

public class TernarySalutation {

	/**
	 * Alle m�glichen Anreden ausgeben
	 */

	private static void testSalutation() {
		boolean bools[] = { true, false };
		for (boolean firstNameBase : bools) {
			for (boolean female : bools) {
				System.out.println(salutation(firstNameBase, female, "Kim", "May"));
			}
		}
	}

	/**
	 * Anrede abh�ngig vom Geschlecht und Bekanntheitsgrad ausgeben
	 * 
	 * @return Anrede
	 * @param onFirstNameBase Per-Du?
	 * @param female          Weiblich?
	 * @param given           Vorname
	 * @param name            Nachname
	 */
	// Die Methode saluation gibt die Anrede abh�ngig vom Geschlecht und
	// Bekanntheitsgrad ausgeben
	static String salutation(boolean onFirstNameBase, boolean female, String given, String name) {

		// Bedingungsoperator ist eine Abk�rzung f�r if esle
		return onFirstNameBase ? (female ? "liebe " + given + "," : "lieber " + given + ",")
				: (female ? "Sehr geehrte Frau " + name + "," : "Sehr geehrter Herr " + name + ",");

	}

	public static void main(String[] args) {
		testSalutation();
	}

}
