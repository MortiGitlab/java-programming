package blatt07;

public class SimplifyOrTransform {

	/**
	 * Formen Sie bitte die folgenden Ausdr�cke etc. in vereinfachte Varianten um:
	 */
	static void toBeSimplified() {
		int a, b, c;

		// Beispiel: Mehrfache Initialisierung mit dem selben Wert:
		a = 0;
		b = 0;
		c = 0;
		// Vereinfacht:
		a = b = c = 0;

		a = (b = 1 + (c = 0));
		// Vereinfacht:
		// TODO (Bitte durch Sie zu entwickeln)
		a = b = 1;
		c = 0;

		a = b++ - (b += 2) - (b = 0);
		// Vereinfacht:
		// TODO (Bitte durch Sie zu entwickeln)
		a = -3;
		b = 0;

		// �ndern Sie bitte ab auf, wo passend, Dekrement/Inkement-Opratoren
		// oder kombinierte Zuweisungsoperatoren (jeweils ohne die Ausgabe zu �ndern
		// ...)
		//
		final int N0 = 0, N1 = 17;
		for (int i = N0; i < N1; i++) {
			System.out.print(i + ", ");
		}

		System.out.println();
		for (int i = N1 - 1; i >= N0; i--) {
			System.out.print(i + ", ");
		}

		System.out.println();
		for (int i = N0; i < N1; i += 2) {
			System.out.print(i + ", ");
		}

		System.out.println();
		for (int i = N1 - 1; i >= N0; i -= 2) {
			System.out.print(i + ", ");
		}

		System.out.println();
		for (int i = N0 + 1; i < N1 * N1; i += i) {
			System.out.print(i + ", "); // warum wird bei dieser for schleife immer mal 2 gerechnet !
		}
		System.out.println();

		// Formen Sie bitte die folgende while-Schleifen in for-Schleifen um
		// und verwenden Sie wieder Inkrementoperatoren/kombinierte Zuweisungsoperatoren
		
		// warum 0 raus kommt ? 
//		a = 1;
//		final int NMAX = 10;
//		while (a < NMAX) {
//			b = 1;
//			while (b < NMAX) {
//				System.out.printf("%3d", a * b);
//				b = b + 1;
//			}
//			System.out.println();
//			a = a + 1;
//		}
//
//		System.out.println();
		
		
		final int NMAX = 10;
		for (int aa = 1; aa < NMAX; aa++) {
			for (int bb = 1; bb < NMAX; bb++) {
				System.out.printf("%3d", a * b);

			}
			System.out.println();

		}

		System.out.println();
		
		
		

// ########### while in for schleife umwandeln

		for (int aa = 3; aa < 100; aa += 2) {

			int bb = 2;

			for (; bb * bb < aa && aa % bb != 0; bb++) {

			}

			if (bb * bb > aa) {
				System.out.print(aa + ", ");
			}

		}

//		a = 3;
//		while (a < 100) {
//			b = 2;
//			while (b * b < a && a % b != 0) {
//				b = b + 1;
//			}
//			if (b * b > a) {
//				System.out.print(a + ", ");
//			}
//			a = a + 2;
//		}

		// Transformieren Sie bitte folgende If-/Else-Kette in eine Switch-Anweisung
		int zufallsZahl = 1 + (int) (Math.random() * 6.);

//		String seite;
//		if (zufallsZahl == 1) {
//			seite = "Eins";
//		} else if (zufallsZahl == 2) {
//			seite = "Zwei";
//		} else if (zufallsZahl == 3) {
//			seite = "Drei";
//		} else if (zufallsZahl == 4) {
//			seite = "Vier";
//		} else if (zufallsZahl == 5) {
//			seite = "F�nf";
//		} else if (zufallsZahl == 6) {
//			seite = "Sechs";
//		} else {
//			seite = "Ung�ltig";
//		}
//		System.out.println("\nGew�rfelt: " + seite);

		switch (zufallsZahl) {
		case 1:
			System.out.println("\nGew�rfelt: Eins");

			break;
		case 2:
			System.out.println("\nGew�rfelt: Zwei");

			break;
		case 3:
			System.out.println("\nGew�rfelt: Drei");

			break;
		case 4:
			System.out.println("\nGew�rfelt: Vier");

			break;
		case 5:
			System.out.println("\nGew�rfelt: F�nf");

			break;
		case 6:
			System.out.println("\nGew�rfelt: Sechs");

			break;

		default:
			System.out.println("ung�ltig");

			break;
		}

	}

	public static void main(String[] args) {
		toBeSimplified();
	}

}
