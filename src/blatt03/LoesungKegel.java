package blatt03;
import java.awt.*;
import javax.swing.JFrame;

/**
 * Klasse, um ein Raster zu malen
 */
public class LoesungKegel extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Konstruktor. Nichts zu �ndern hier */
    public LoesungKegel(){
        super();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(600,417);
        this.setTitle("L�sung in der Klasse " + this.getClass().getName()); 
        this.setVisible(true);
    }

    /** Main-Methode. Nichts zu �ndern hier. */
    public static void main(String[] args){
    	new LoesungKegel();
    }
   
    /** Paint-Methode. Nichts zu �ndern hier. Diese Methode 
     *  wird beim �ffnen des Fensters automatisch aufgerufen. 
     *  Die Methode wird auch aufgerufen, wenn die Gr��e des Fensters
     *  ver�ndert wird. 
     *  @param g Grafik, auf die gezeichnet wird.
     *  */
    public void paint(Graphics g){
    	/* L�schen des Fensterinhalts (wichtig bei erneutem Zeichnen): */
    	g.clearRect(0, 0, this.getWidth(), this.getHeight());
    	
    	
    	this.meinKegel(g); // Aufruf der selbst programmierten Methode
    }
   
	/** 
	 * Hier bitte die L�sung, um den Kegel zu zeichnen
	 * 
	 */
   public void meinKegel(Graphics g){
	   // TODO
	   
		int x25 = this.getWidth() / 4;		// x-Koordinate bei 25% der Fensterbreite (von links)
		int x75 = this.getWidth() * 3 / 4;  // x-Koordinate bei 75% der Fensterbreite (von links)
		int y25 = this.getHeight() / 4;		// y-Koordinate bei 25% der Fensterbreite (von oben)
		int y75 = this.getHeight() * 3 / 4; // y-Koordinate bei 75% der Fensterbreite (von oben)

		// TODO: Komplettes Rechteck zeichnen
		// das sind koordinatensysteme im Internet  mit Drawing a line 
		//https://www.androidberry.com/java/java-drawing-lines-rectangles.html
		g.drawLine(x75, y25, x25, y25);
		//g.drawLine(x25, y25, x25, y75);
		//g.drawLine(x75, y75, x75, y25);// einfach koordinaten ablesen 
		g.drawLine(x25, y75, x75, y75);
		
		
		double x25a = ((double) this.getWidth()) / 4;		// x-Koordinate bei 25% der Fensterbreite (von links)
		double  x75b= ((double) this.getWidth() )* 3 / 4;  // x-Koordinate bei 75% der Fensterbreite (von links)
		int anzahl=40;
		double abstand=(x75b-x25a)/(anzahl+1);
		double x=x25;
		double x2=x75;
		for (int i=0;i<anzahl+2;i++) {
			
	if(i==anzahl+1) {
				
				g.setColor(Color.black);
				
				
			}
		
			// TODO: rote Diagonale zeichnen
			//g.setColor(Color.r); 
			g.drawLine((int)x, y25, (int)x2, y75);
			g.setColor(Color.lightGray);
			x=abstand + x;
			x2=x2-abstand;
			
			
			//g.drawLine(x, y25, x25, y75);
			
			
			
		}

	
	   
   } 
}