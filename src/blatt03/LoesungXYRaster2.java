package blatt03;
import java.awt.*;
import javax.swing.JFrame;

/**
 * Klasse, um ein Raster zu malen
 */
public class LoesungXYRaster2 extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Konstruktor. Nichts zu �ndern hier */
    public LoesungXYRaster2(){
        super();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(900,500);
        this.setTitle("L�sung in der Klasse " + this.getClass().getName()); 
        this.setVisible(true);
    }

    /** Main-Methode. Nichts zu �ndern hier. */
    public static void main(String[] args){
    	new LoesungXYRaster2();
    }
   

    /** Paint-Methode. Nichts zu �ndern hier. Diese Methode 
     *  wird beim �ffnen des Fensters automatisch aufgerufen. 
     *  Die Methode wird auch aufgerufen, wenn die Gr��e des Fensters
     *  ver�ndert wird. 
     *  @param g Grafik, auf die gezeichnet wird.
     *  */
    public void paint(Graphics g){
    	/* L�schen des Fensterinhalts (wichtig bei erneutem Zeichnen): */
    	g.clearRect(0, 0, this.getWidth(), this.getHeight());
    	
    	
    	this.meinRaster(g);
    }// Aufruf der selbst programmierten Methode
    
	/** 
	 * Hier bitte die L�sung, um das Raster zu zeichnen
	 * 
	 */
   public void meinRaster(Graphics g){
	
	   
		int x25 = this.getWidth() / 4;		// x-Koordinate bei 25% der Fensterbreite (von links) 600/4=150
		int x75 = this.getWidth() * 3 / 4;  // x-Koordinate bei 75% der Fensterbreite (von links) 600* 3 / 4=450
		int y25 = this.getHeight() / 4;		// y-Koordinate bei 25% der Fensterbreite (von oben) 400/4=100
		int y75 = this.getHeight() * 3 / 4; // y-Koordinate bei 75% der Fensterbreite (von oben)400* 3 / 4=300

	
		final int lines = 10; // Die Anzahl an Linien, die gezeichnet werden m�ssen (11)
		
		
		double bereite = this.getWidth() / (double)2 / lines; //breite 
		double hoehe = this.getHeight() / (double)2 / lines;//h�he
		
		
		
		for(int i=0;i<=lines;i++) {
			
			if(i%2 == 0)//
			{
				
				
			g.setColor(Color.BLACK);
			}
			
			
			else
			{
				
				
			g.setColor(Color.LIGHT_GRAY);
			
			}
			
			
			int abstandNeuY = y25 + (int)(i * hoehe);
			g.drawLine(x25, abstandNeuY, x75, abstandNeuY);
			
			
			
			int abstandNeuX = x25 + (int)(i * bereite);
			g.drawLine(abstandNeuX, y25, abstandNeuX, y75);
			
			
			}
			}
	
		
	

   }
		
