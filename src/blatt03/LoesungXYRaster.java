package blatt03;
import java.awt.*;
import javax.swing.JFrame;

/**
 * Klasse, um ein Raster zu malen
 */
public class LoesungXYRaster extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Konstruktor. Nichts zu �ndern hier */
    public LoesungXYRaster(){
        super();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(900,500);
        this.setTitle("L�sung in der Klasse " + this.getClass().getName()); 
        this.setVisible(true);
    }

    /** Main-Methode. Nichts zu �ndern hier. */
    public static void main(String[] args){
    	new LoesungXYRaster();
    }
   

    /** Paint-Methode. Nichts zu �ndern hier. Diese Methode 
     *  wird beim �ffnen des Fensters automatisch aufgerufen. 
     *  Die Methode wird auch aufgerufen, wenn die Gr��e des Fensters
     *  ver�ndert wird. 
     *  @param g Grafik, auf die gezeichnet wird.
     *  */
    public void paint(Graphics g){
    	/* L�schen des Fensterinhalts (wichtig bei erneutem Zeichnen): */
    	g.clearRect(0, 0, this.getWidth(), this.getHeight());
    	
    	
    	this.meinRaster(g);
    }// Aufruf der selbst programmierten Methode
    
	/** 
	 * Hier bitte die L�sung, um das Raster zu zeichnen
	 * 
	 */
   public void meinRaster(Graphics g){
	   // TODO
	   
		int x25 = this.getWidth() / 4;		// x-Koordinate bei 25% der Fensterbreite (von links) 600/4=150
		int x75 = this.getWidth() * 3 / 4;  // x-Koordinate bei 75% der Fensterbreite (von links) 600* 3 / 4=450
		int y25 = this.getHeight() / 4;		// y-Koordinate bei 25% der Fensterbreite (von oben) 400/4=100
		int y75 = this.getHeight() * 3 / 4; // y-Koordinate bei 75% der Fensterbreite (von oben)400* 3 / 4=300


		
		int schritt=((x75-x25)/10);
		int schritt2=((y75-y25)/10);
		
		// https://www.youtube.com/watch?v=no4m-TIX-rc 
		// x25=150, x75=450
		//erste for schleife gibt die vertikalen Linien raus
		//450-150=300/10=30 
		for(int i=x25;i<=x75;i=i+schritt) {
			
			
			
			if(i%(schritt*2)==0  ) {

				g.setColor(Color.LIGHT_GRAY);
				g.drawLine(i,y25, i, y75);
				
			}
			
			else {
				
				g.setColor(Color.black);
				g.drawLine(i,y25, i, y75);
				
			}			
			
		}
		
		
		
		for (int j=y25;j<=y75;j=j+schritt2) {
			
			
			if(j%(schritt2*2)==0) {
				
				g.setColor(Color.LIGHT_GRAY);

				g.drawLine(x75,j,x25, j);
				
			}
			
			
	else {
				g.setColor(Color.black);
				g.drawLine(x75,j,x25, j);
				
			}
			
			
		}
		
		
		
		
		
		
		
		
  } 
	      
}
