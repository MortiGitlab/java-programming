package blatt06;

/**
 * Pruefen eines 9x9 Sudokus
 * 
 * @author Ulrich.Broeckl@hs-karlsruhe.de,<br>
 *         sclu1042@hs-karlsruhe.de
 */

// 1. Was sind erlaubte Werte im Spielfeld? 0 bis 9 
//2. Welche Bedeutung hat die "0" dabei? 0 fungiert als Platzhalter 
//3. Warum ist die Reihung testSpielFelder dreidimensional? 3 Zeilen und 3 Spalten bzw Array für mehrere Spielfelder  
//4. Was steht in der 1., 2. und 3. Dimension? 
// 5. Welche Dimension hat die Reihung testFeld in der Methode testSudokuChecker? 2 te Dimension
//6. In der Methode testSudokuChecker wird this.spielFeld[8][8] auf
//   8 geändert. Ändert sich dadurch testSpielFelder auch? Wieso (nicht)? ja, da this.spielFeld[8][8]  auf testsodukoChecker zeiget
//7. Wie/wieso funktioniert die For-Each-Schleife in der Methode
//      testSudokuChecker? Was wird hier durchlaufen? wird alle Felder durch for durchsucht

public class Sudokuchecker {
	/**
	 * Wie gross ist ein Block (i.d.R. 3) im Sudoku.<br>
	 * <br>
	 * <b>WARNUNG:</b> Es reicht nicht aus, einfach diese Zahl zum Beispiel fuer
	 * 16x16 Sudokus hochzusetzen. Da ist einiges mehr zu tun ...
	 */
	private final int BLOCK_SIZE = 3;

	/**
	 * Anzahl der Spalten/Zeilen
	 */
	private final int ROW_SIZE = BLOCK_SIZE * BLOCK_SIZE;

	/**
	 * Zweidimensionales Array, welches das initiale (und das geloeste) Sudoku
	 * speichert.<br>
	 * <br>
	 * Moegliche Eintraege:
	 * <ul>
	 * <li>Zahlen 1..9</li>
	 * <li>0 fuer unbekannter Wert</li>
	 * </ul>
	 */
	private int[][] spielFeld;

	// Achtung: Arrays enthalten spaeter Loesung!
	private int[][][] testSpielFelder = {
			/* |---------|---------|---------| */
			{ { 5, 5, 0, 0, 0, 0, 0, 0, 0 }, { 5, 5, 0, 0, 0, 0, 0, 0, 0 }, { 5, 5, 0, 0, 0, 0, 0, 0, 0 },
					/* |---------|---------|---------| */
					{ 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
					/* |---------|---------|---------| */
					{ 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0 } },
			/* |---------|---------|---------| */

			/* |---------|---------|---------| */
			{ { 5, 3, 4, 6, 7, 8, 9, 1, 2 }, { 6, 7, 2, 1, 9, 5, 3, 4, 8 }, { 1, 9, 8, 3, 4, 2, 5, 6, 7 },
					/* |---------|---------|---------| */
					{ 8, 5, 9, 7, 6, 1, 4, 2, 3 }, { 4, 2, 6, 8, 5, 3, 7, 9, 1 }, { 7, 1, 3, 9, 2, 4, 8, 5, 6 },
					/* |---------|---------|---------| */
					{ 9, 6, 1, 5, 3, 7, 2, 8, 4 }, { 2, 8, 7, 4, 1, 9, 6, 3, 5 }, { 3, 4, 5, 2, 8, 6, 1, 7, 0 } },
			/* |---------|---------|---------| */

			/* |---------|---------|---------| */
			{ { 0, 0, 0, 5, 4, 2, 0, 0, 0 }, { 9, 6, 7, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 3, 1, 8 },
					/* |---------|---------|---------| */
					{ 0, 0, 0, 0, 7, 0, 8, 6, 4 }, { 0, 2, 0, 6, 0, 4, 0, 9, 0 }, { 6, 4, 5, 0, 1, 0, 0, 0, 0 },
					/* |---------|---------|---------| */
					{ 8, 9, 1, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 5, 4, 7 }, { 0, 0, 0, 3, 2, 6, 0, 0, 0 } },
			/* |---------|---------|---------| */

	};

	/** Testmethode */
	public void testSudokuChecker() {
		for (int[][] testFeld : this.testSpielFelder) {
			this.spielFeld = testFeld;

			// Korrekte Spielfelder pruefen
			this.print();
			System.out.println("Falscher Alarm?");
			System.out.println("Womoeglich doppelter Wert auf [8][8] ...");

			this.validiereSpielfeld();

			// Fehler einbauen
			this.spielFeld[8][8] = -1;
			this.print();
			System.out.println("Falscher Wert auf [8][8] ...");
			this.validiereSpielfeld();

			this.spielFeld[8][8] = 8;
			this.print();
			System.out.println("Womoeglich doppelter Wert auf [8][8] ...");
			this.validiereSpielfeld();
		}
	}

	/**
	 * Pruefen, ob ein gegebener Wert im erlaubten Bereich 1 ... ROW_SIZE ist.
	 * 
	 * @param wert Wert, in 1 ... ROW_SIZE sein soll
	 * @return true, falls Wert in Ordnung
	 */
	private boolean isValueOk(int wert) {

		if ((wert >= 1) && (wert <= ROW_SIZE)) {
			return true;

		} else {
			return false;
		}

	}

	/**
	 * Pruefen, ob ein gegebener Wert an gegebener Position in einer Zeile erlaubt
	 * ist.
	 * 
	 * @param zeile Zeile, in der der Wert geprueft wird
	 * @param wert  Wert, der noch nicht in der Zeile vorhanden sein darf
	 * @return true, falls Wert noch nicht vorhanden.
	 */
	private boolean isZeileOk(int zeile, int wert) {
		// TODO
		for (int i : spielFeld[zeile]) {
			if (i == wert) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Pruefen, ob ein gegebener Wert in einer Spalte erlaubt ist.
	 * 
	 * @param spalte Spalte, in der der Wert geprueft wird
	 * @param wert   Wert, der noch nicht in der Spalte vorhanden sein darf
	 * @return true, falls Wert noch nicht vorhanden.
	 */
	private boolean isSpalteOk(int spalte, int wert) {
		// TODO

		for (int[] is : spielFeld) {
			if (is[spalte] == wert) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Pruefen, ob ein gegebener Wert in einem Block erlaubt ist.
	 * 
	 * @param zeile  Zeile, in der der Wert geprueft wird
	 * @param spalte Spalte, in der der Wert geprueft wird
	 * @param wert   Wert, der noch nicht in dem Block vorhanden sein darf
	 * @return true, falls Wert noch nicht vorhanden.
	 */
	private boolean isBlockOk(int zeile, int spalte, int wert) {
		// TODO#
		// 8 plätze reservieren
		int[] block = new int[8];
		// Auf Anfang des Blocks gehen
		int PosX = zeile % 3;
		int PosY = spalte % 3;
		PosX = zeile - PosX;// immer kommt null raus
		PosY = spalte - PosY;// immer kommt null raus
		// ---------------------------

		// jetzt prüfen durch for schleife
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				block[i * j] = spielFeld[PosX + i][PosY + j];

			}
		}

		for (int i : block) {
			if (i == wert) {
				return false;
			}

		}

		return true;
	}

	/**
	 * Ein Sudoku pruefen, ob keine Werte doppelt in Zeilen, Spalten oder Bloecken
	 * auftauchen. Falls doch, einfach ausgeben, wo das der Fall ist.
	 */
	public void validiereSpielfeld() {
		for (int iZeile = 0; iZeile < ROW_SIZE; iZeile++) {
			for (int iSpalte = 0; iSpalte < ROW_SIZE; iSpalte++) {

				int wert = spielFeld[iZeile][iSpalte];

				if (wert != 0) { // Feld besetzt

					spielFeld[iZeile][iSpalte] = 0; // Damit eigentlicher Wert nicht wie Doppelung aussieht

					if (!isValueOk(wert)) {
						System.out.format("Ungueltiger Wert %d in Zeile %d, Spalte %d! \n", wert, iZeile + 1,
								iSpalte + 1);
					}
					if (!isZeileOk(iZeile, wert)) {
						System.out.format("Doppelter Zeilenwert %d in Zeile %d, Spalte %d! \n", wert, iZeile + 1,
								iSpalte + 1);
					}
					if (!isSpalteOk(iSpalte, wert)) {
						System.out.format("Doppelter Spaltenwert %d in Zeile %d, Spalte %d! \n", wert, iZeile + 1,
								iSpalte + 1);
					}
					if (!isBlockOk(iZeile, iSpalte, wert)) {
						System.out.format("Doppelter Blockwert %d in Zeile %d, Spalte %d! \n", wert, iZeile + 1,
								iSpalte + 1);
					}

					spielFeld[iZeile][iSpalte] = wert; // Wert wieder herstellen
				} // Feld besetzt
			}
		}
	}

	/**
	 * Ein Sudoku ausgeben.
	 */
	public void print() {
		final String horizBorder = " ┼────────┼────────┼────────┼";

		System.out.println();

		for (int iZeile = 0; iZeile < ROW_SIZE; iZeile++) {
			if (0 == iZeile % 3) {
				System.out.println(horizBorder);
			}

			for (int iSpalte = 0; iSpalte < ROW_SIZE; iSpalte++) {
				if (0 == iSpalte % 3) {
					System.out.print(" │ ");
				}

				int wert = spielFeld[iZeile][iSpalte];
				if (wert == 0) {
					System.out.print("_ ");
				} else {
					System.out.print(wert + " ");
				}

			}
			System.out.println(" │");
		}
		System.out.println(horizBorder);
	}

	public static void main(String[] args) {

		new Sudokuchecker().testSudokuChecker();
	}

}
