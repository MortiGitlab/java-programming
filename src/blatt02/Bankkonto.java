/**
 * 
 */
package blatt02;

/**
 * @author Morteza Hosseini
 * 
 */
public class Bankkonto {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		double guthabenInEuro=0.0 ;
		int Jahre=30; //Dauer des Sparplanes
		double zins =0.02;
		
		
		for (int i=1;i<=Jahre;i++) {
			
		    guthabenInEuro=(guthabenInEuro*zins)+guthabenInEuro;
		    guthabenInEuro=guthabenInEuro+1000;
			System.out.println("Guthaben nach dem "+i+". Jahr : "+guthabenInEuro);	
		}
	}

}
