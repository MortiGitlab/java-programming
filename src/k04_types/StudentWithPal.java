	
	// Datei Student.java
	package k04_types;
	
	/**
	 * Klasse  f�r Studierende, die mit einem anderen Studierenden zusammenarbeiten
	 */
	public class StudentWithPal extends Student {
		/** Mit wem  zusammengearbeitet wird */
		StudentWithPal pal;
		/**
		 * Konstruktor
		 * @param vorname
		 * @param nachname
		 * @param matrNr
		 */
		public StudentWithPal(String vorname, String nachname, int matrNr) {
			super(vorname, nachname, matrNr);
			this.pal = null;
		}
	
		
		public void setPal(StudentWithPal pal) {
			this.pal = pal;
			this.pal.pal = this; // Bidirektional setzen
		}
	
		
		public void print() {
			super.print();
			if(null != this.pal) {
				System.out.print( " arbeitet mit: ");
				this.pal.pal = null; // Endlosrekursion vermeiden (kurz trennen)
				this.pal.print();
				this.pal.pal = this; // Wieder verfreunden 
			}
				
		}
		/**
		 * Tests
		 * @param args unused
		 */
		public static void main(String[] args) {
			StudentWithPal s1 = new StudentWithPal("Kai", "Hai", 4711);
			StudentWithPal s2 = new StudentWithPal("May", "Lin", 4712);
			s1.print(); System.out.println();
			s2.print(); System.out.println();

			s1.setPal(s2);
			s1.print(); System.out.println();
			s2.print(); System.out.println();
		}
	}