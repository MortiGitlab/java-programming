	
	// Datei Tutor.java
	package k04_types;

/**
	 * Simple class for students
	 */
	public class Tutor extends Student {
		protected int matrNr;
		private Lecturer lecturer;
		/**
		 * Constructor
		 * @param vorname
		 * @param nachname
		 * @param matrNr
		 * @param lecturer
		 */
		public Tutor(String vorname, String nachname, int matrNr, Lecturer lecturer) {
			super(vorname, nachname, matrNr);
			this.setLecturer(lecturer);
		}
		
		protected Lecturer getLecturer() {
			return lecturer;
		}
		protected void setLecturer(Lecturer lecturer) {
			this.lecturer = lecturer;
		}
		
		public void print() {
			super.print();
			if(null != this.getLecturer())
			{
				System.out.print("Tutor bei: ");
				this.getLecturer().print();
			}
		}
		/**
		 * Tests
		 * @param args unused
		 */
		public static void main(String[] args) {
			Tutor t = new Tutor("Kai", "Hai", 4711, new Lecturer("Horst", "Held", "E234"));
			t.print(); System.out.println();
			t = new Tutor("Nina", "Mai", 4712, t.getLecturer());
			t.print();
		}
	}