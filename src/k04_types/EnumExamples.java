// Datei EnumExamples.java
package k04_types;

// Enum des Pakets
enum Wochentag {
	Mo, Di, Mi, Do, Fr, Sa, So
}

public class EnumExamples {
	public static void main(String[] args) {
		// Lesbarer
		Wochentag heute = Wochentag.Mo;
		// ...
		boolean segelnGehn = (heute == Wochentag.Sa || heute == Wochentag.So);
		if (segelnGehn)
			System.out.println("Hurra, wir gehen segeln!\n");

		// Enums (als String!) ausgeben:
		System.out.println(Wochentag.Mi); // --> Mi

		// Enums sind auch Klassen:
		System.out.println(Wochentag.class);
		// --> class types.Wochentag

		// Enums und Reihungen:
		String engl[] = new String[Wochentag.values().length];
		engl[Wochentag.Mo.ordinal()] = "Monday";
		engl[Wochentag.Di.ordinal()] = "Tuesday"; // ..

		// Enums als Reihungen:
		Wochentag[] tage = Wochentag.values();
		for (Wochentag tag : tage)
			System.out.printf("%d. Tag: %s, ", tag.ordinal() + 1, tag);
		// --> 1. Tag: Mo, 2. Tag: Di, 3. Tag: ... Sa, 7. Tag: So,
		System.out.println();

		for (WochentagWE tag : WochentagWE.values()) {
			String weSign;
			if (tag.isWeekend())
				weSign = "(*),";
			else
				weSign = ",";
			System.out.printf("%s.%s ", tag, weSign);
		} // --> Mo., Di., Mi., Do., Fr., Sa.(*), So.(*),
	}

}

/**
 * Wochentag mit Info, ob Wochenendtag
 */
enum WochentagWE {
	Mo(false), Di(false), Mi(false), Do(false), Fr(false), Sa(true), So(true);

	private boolean isWeekend;

	private WochentagWE(boolean isWeekend) {
		this.isWeekend = isWeekend;
	}

	public boolean isWeekend() {
		return this.isWeekend;
	}
}
