
// Datei Temperature1.java
package k04_types;

public class Temperature1 {
	private float celsius; // Temperatur in �C

	/**
	 * Konstruktor
	 * 
	 * @param celsius Temperatur in �C
	 */
	public Temperature1(float celsius) {
		this.celsius = celsius;
	}

	/**
	 * @param celsius Temperatur in �C
	 */
	public void setTemperature(float celsius) {
		this.celsius = celsius;
	}

	/**
	 * @return Temperatur in �C
	 */
	public float getTemperature() {
		return this.celsius;
	}

	public static void main(String[] args) {
		Temperature1 t1 = new Temperature1(100);
		// | | | |
		// [Typ] [Name] [Zuweisung] [Aufruf Konstruktor]

		System.out.println(t1.getTemperature());

		t1.setTemperature(55);
		System.out.println(t1.getTemperature());

		Temperature1 t2 = new Temperature1(44);
		System.out.println(t2.getTemperature());
	}
}
