		
	package k04_types;
	
	public class MultiDimArraysExamples {
		static void initializingExamples() {
			int[][] zweidim1 = new int [4][2];
			zweidim1[1][1] = 11; // nogo: twodim1[1,1]=11
			
			int[][] zweidim2 = new int [2][];
			zweidim2[1] = new int[3];
			zweidim2[1][1] = 11;
			zweidim2[1][2] = 12;
			
			String[][][] eBau = {
					{ // UG
						{ // S�d
							"EU01", "EU02", "EU03", "EU04", 
						},
						{ // Mitte
							"WC", "Putzraum", "Dusche", 						},
						{ // Nord
							"EU11", "EU12", "EU13", "EU14", 
						}
					}, 
					{ // EG
						{ // S�d
							"E001", "E002", "E003", "E004", 
						},
						{ // Nord
							"E011", "E012",         "E014", 
						}
					}
			}; 
			eBau[0][0][1] = "InfoLabRaum";
		}
	
		static void twoDimExample() {
			byte bArr[][] = { 
					{ 0, 0, 0, 0, 0, 0, }, 
					{ 0, 1, 0, 0, 0, 0, }, 
					{ 1, 2, 1, 0, 0, 0, }, 
					{ 0, 1, 0, 0, 0, 0, },
					{ 0, 0, 0, 0, 0, 0, }, 
					{ 0, 0, 0, 0, 0, 0, }, 
					{ 0, 0, 0, 0, }, 
					{ 0, 0, 0, 0, 0, 0, },
					{ 0, 0, 0, 0, 0, 0, }, 
					{ 0, 0, 0, 0, 0, 0, }, 
					{ 0, 0, 0, 0, 0, 0, },
	
			};
			// Wo steckt die 2?
			System.out.println(bArr[1][2]); // Da?
			System.out.println(bArr[2][1]); // Oder da?
	
			// Die 2 Suchen:
			int iX, iY;
			for (iY = 0; iY < bArr.length; iY = iY + 1) {
				for (iX = 0; iX < bArr[iY].length; iX = iX + 1) {
					if (bArr[iY][iX] == 2)
						System.out.println("Gefunden auf x=" + iX + " y=" + iY);
				}
			}
		}
	
		public static void main(String[] args) {
			initializingExamples();
			twoDimExample();
		}
	}
