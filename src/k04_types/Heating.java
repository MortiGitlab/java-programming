
// Datei Heating.java 
package k04_types;

/**
 * Einfache Klasse, um Heizung an- oder auszuschalten
 */
public class Heating {
	protected boolean state = false; // false == aus, true == an

	/** Heizung ausschalten */
	public void off() {
		this.state = false;
	}

	/** Heizung anschalten */
	public void on() {
		this.state = true;
	}

	/** Zustand der Steuerung ausgeben */
	public void print() {
		if (state) {
			System.out.print("Heizung an.  ");
		} else {
			System.out.print("Heizung aus. ");
		}
	}
}