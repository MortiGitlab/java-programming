package k04_types;

public class Entfernungen {

	String[] staedte = { "MA", "KA", "S" };
	double[][] entfernungen = {

			{ 0., 70., 139. }, // Ma
			{ 70., 0., 84. }, // ka
			{ 139., 48., 0. },// s

	};

	double getEntfernung(int von, int nach) {
		double ret = -1;
		if (von < 0 || nach < 0 || von >= staedte.length || nach >= staedte.length) {
			System.out.println("besser auf parameter achten ... ");
		} else {
			ret = this.entfernungen[von][nach];
		}
		return ret;
	}

	public static void main(String[] args) {
		Entfernungen e = new Entfernungen();
		System.out.println(e.getEntfernung(0, -1));

		// TODO Auto-generated method stub

	}

}
