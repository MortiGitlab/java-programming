
// Datei Person3.java 
package k04_types;

/**
 * Einfache Klasse f�r Personen mit Druckz�hler
 */
public class Person3 {
	protected String vorname; // Instanzvariable
	protected String nachname; // Instanzvariable

	public Person3(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}

	static int numPrints = 0; // Klassenvariable nur genau einmal , hochz�hlen von nummern, mitz�hlen von Aufrufen von 
	// Methoden, kosten abrechnung 
	final static int ANSWER_TO_ALL_QUESTIONS = 42; // Klassenvariable als globale Konstante
													// Aufruf durch: Person3.ANSWER_TO_ALL_QUESTIONS

	public void print() {
		// Z�hler erh�hen
		Person3.numPrints = Person3.numPrints + 1;
		System.out.printf("%10s, %-10s (DruckNr. %d)\n", nachname, vorname, Person3.numPrints);
	}

	public static void main(String[] args) {
		Person3 p1 = new Person3("Thea", "Mi");
		Person3 p2 = new Person3("Kai", "Hai");
		p1.print();
		p2.print();
		p2.print();
		p1.print();
	}
}