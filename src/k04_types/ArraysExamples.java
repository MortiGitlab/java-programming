	
	package k04_types;
	
	public class ArraysExamples {
		
		public static void main(String[] args) {
			char cArr[]; // Deklaration
			cArr = new char[6]; // Array erzeugen
			cArr[0] = 'B'; // Initialisierung
			cArr[1] = 'e'; // ...
	
			// Array ist immer ein Objekt, nicht nur Speicher wie in C:
			System.out.println(cArr.getClass().getCanonicalName());
	
			// (Teils fehlerhafte) Beispiele des indizierten Zugriffs
			char c = cArr[0]; // OK
			// nogo c = cArr[-1];
			int idx = -1;
			// nogo c = cArr[idx]; // Negativer Index
			// nogo c = cArr[6]; // Index zu gro�
			// nogo c = cArr[7]; // Erst recht zu gro�
			cArr[cArr.length - 1] = 'Z'; // Elegant das letzte Element setzen
	
			// Erzeugen mit Initialisierungsliste
			char[] cArr2 = { 'a', 'c', 'a', 'c', };
	
			// Davon durchschnittlichen Buchstaben ausrechnen:
			int sum = 0;
			for (idx = 0; idx < cArr2.length; idx = idx + 1) {
				sum = sum + cArr2[idx];
			}
			System.out.println("Durchschnittlicher Buchstabe: " + 
					(char) (sum / cArr2.length));
	
			// Klassen-Typen als Array-Elemente
			Person persons[] = new Person[6];
			persons[0] = new Person("Lin", "May");
			persons[1] = new Person("Kai", "Hai");
	
			for (int i = 0; i < persons.length; i = i + 1) {
				System.out.println(persons[i]);
			}
	
			// Moderner so:
			Person[] persons2 = { 
					new Person("Lin", "May"), new Person("Kai", "Hai"), 
					null, null, null, null };
			for (Person p : persons2) {
				System.out.println(p);
			}
			arr2csv_test(); 
		}

		
		
		
		
		
		
		
		
		// Bonus-Track: Arrays als Parameter
		// Aufruf: arr2csv_test();
		static void printArrayAsCsv(Object[] arr) {
			for(Object s : arr)
				System.out.print( s + ",");
			System.out.println();
		}
		static void arr2csv_test() {
			String[] arr = new String[2];
			arr[0] = arr[1] = "Blah";
			printArrayAsCsv(arr);
			printArrayAsCsv( new Object[] {4711, "Blubb", '�'}	);
		}
	
	}
