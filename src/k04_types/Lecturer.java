	
	// Datei Lecturer.java
	package k04_types;
	
	/**
	 * Einfache Klasse f�r Dozierende
	 */
	public class Lecturer extends Person {
		protected String roomNr;
	
		/**
		 * Constructor
		 * @param vorname
		 * @param nachname
		 * @param roomNr
		 */
		public Lecturer(String vorname, String nachname, String roomNr) {
			super(vorname, nachname);
			this.roomNr = roomNr;
		}
		
		public void print() {
			super.print();
			System.out.printf("[%s] ", this.roomNr);
		}
		/**
		 * Tests
		 * @param args unused
		 */
		public static void main(String[] args) {
			Lecturer l = new Lecturer("Ulrich", "Br�ckl", "E212");
			l.print();
			Student s = new Student("Kai", "Hai", 4711);
			s.print();
			
			// Polymorphie (Vielgestaltigkeit):
			System.out.println();
			Person p = l;
			p.print();
			p = s;
			p.print();
			p = new Student("Nina", "Kern", 4712);
			p.print();

		}	}