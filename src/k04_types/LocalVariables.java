
package k04_types;

public class LocalVariables {
	static int answer = 42; // Klassenvariable

	/** Methode um kurz ein int-Variable auszugeben */
	static void printVar(String name, int wert) {
		System.out.printf("\n%8s: %d", name, wert);
	}

	public static void main(String[] args) {
		Person p = null;
		int i0 = 0;
		printVar("i0", i0);
		// nogo: printVar("i1", i1); // P0

		if (i0 == 0) {
			printVar("\ti0", i0);
			int i1 = 1;
			printVar("\ti1", i1); // PX
			if (i1 == 1) {
				printVar("\t\ti0", i0);
				printVar("\t\ti1", i1);
				int i2 = 2;
				printVar("\t\ti2", i2);
				p = new Person("In", "Halde"); // P2
			}
			// nogo: printVar("\ti2", i2);
			printVar("\ti1", i1);
		}
		// P1
		// nogo: printVar("i1", i1);
		printVar("i0", i0);

		// Die Person p lebt immer noch!
		System.out.println('\n');
		p.print();
	} // P0'

}
