
	// Datei Student.java
	package k04_types;
	
	/**
	 * Einfache Klasse f�r Studierende
	 */
	public class Student extends Person {
		protected int matrNr;
	
		/**
		 * Konstruktor
		 * @param vorname
		 * @param nachname
		 * @param matrNr
		 */
		public Student(String vorname, String nachname, int matrNr) {
			super(vorname, nachname);
			this.matrNr = matrNr;
		}
	
		
		public void print() {
			super.print();
			System.out.printf("(%05d) ", this.matrNr);
		}
		/**
		 * Tests
		 * @param args unused
		 */
		public static void main(String[] args) {
			Student p = new Student("Kai", "Hai", 4711);
			p.print();
		}
	}