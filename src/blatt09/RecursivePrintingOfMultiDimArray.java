package blatt09;

public class RecursivePrintingOfMultiDimArray {
	static String[][][] eBau = { { // UG
			{ // S�d
					"EU01", "EU02", "EU03", "EU04", },
			{ // Mitte
					"WC", "Putzraum", "Dusche", },
			{ // Nord
					"EU11", "EU12", "EU13", "EU14", } },
			{ // EG
					{ // S�d
							"E001", "E002", "E003", "E004", },
					{ // Nord
							"E011", "E012", "E014", } } };
	static String[] simple = { "a", "b" };

	/**
	 * Eine Reihung, die rekursiv wieder Reihungen enthalten kann, ausgeben
	 * 
	 * @param objs Reihung, die Reihungen oder Zeichenketten enth�lt
	 */
	static void printRecursively(Object[] objs) {
		for (Object obj : objs) {
			if (obj.getClass().isArray()) {
				printRecursively((Object[]) obj);
			} else {
				System.out.println(obj.toString().trim());
			}
		}
	}

	/**
	 * Eine Reihung, die rekursiv wieder Reihungen enthalten kann, als Xml ausgeben
	 * 
	 * @param objs   Reihung, die Reihungen oder Zeichenketten enth�lt
	 * @param indent Wie viel soll bei der Ausgabe einger�ckt werden. Entspricht der
	 *               Rekursionstiefe.
	 */

	static void printInXml(Object[] objs, String indent) {

		for (Object o : objs) {
			if (o.getClass().isArray()) {
				System.out.println(indent + "<container>");
				printInXml((Object[]) o, indent + "\t");
				System.out.println(indent + "</container>");
			} else {
				System.out.println(indent + "\t<string>" + o + "</string>");
			}
		}

	}

	public static void main(String[] args) {

		printInXml(eBau, ""); // Zu programmierende Variante
		// RecursivePrintingOfMultiDimArray.printRecursively(RecursivePrintingOfMultiDimArray.eBau);

	}
}
